/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dibujar;

import java.awt.Point;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SplitMenuButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

public class FXMLDocumentController {

    @FXML
    private AnchorPane root;
    @FXML
    private Pane botones;
    @FXML
    private Button linea;
    @FXML
    private Button cuadri;
     @FXML
    private Button limpiar;
    @FXML
    private ColorPicker colorPicker;
    @FXML
    private SplitMenuButton tamanio;
    @FXML
    private MenuItem uno;
    @FXML
    private MenuItem dos;
    @FXML
    private MenuItem tres;
    @FXML
    private MenuItem cuatro;
    
    ArrayList<String[]> asa = new ArrayList<>();
    int sas = 1;
    int contador = 0;
    int grosor =1;
    Bresenham d = new Bresenham();
    String opcion="";
    String[] efe = new String[2];
    Color color = Color.BLACK;
    
    @FXML
    public void initializable(){
        
    }
    
    public void limpiar(){
        
        root.getChildren().clear();
        root.getChildren().add(botones);
        drawLines();
        asa.clear();
        
    }

    public Color colorrcito(){
        return colorPicker.getValue();
    }
    
    public Color selectColor(){
        Color var = colorrcito();
        if (var == Color.WHITE){
            return Color.BLACK;
        }
        return var;
    }
    
    public int Grosor(){
        uno.setOnAction((e)-> {
            System.out.println("Choice 1 selected");
            grosor=1;
        });
        dos.setOnAction((e)-> {
            System.out.println("Choice 2 selected");
            grosor=2;
        });
        tres.setOnAction((e)-> {
            System.out.println("Choice 3 selected");
            grosor=3;
        });
        cuatro.setOnAction((e)-> {
            System.out.println("Choice 4 selected");
            grosor=4;
        });
        
        return grosor;
    }
    
    public void dibujar(){
        root.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if(opcion.equals("Trazos")){
                    String[] aux = new String[2];
                    aux[0]=String.valueOf(event.getSceneX());
                    aux[1]=String.valueOf(event.getSceneY());
                    dibujarRect(Double.parseDouble(String.valueOf(event.getSceneX())), Double.parseDouble(String.valueOf(event.getSceneY())));
                    asa.add(aux);
                    if(sas%2==0){
                        for(String[] ax : asa){
                            System.out.println("x="+ax[0]+" y="+ax[1]);
                        }
                        drawRectangle();
                        sas = 1;
                        asa.clear();
                    }else{
                        sas++;
                    }
                }else if(opcion.equals("Lineas")){
                    String[] nes = new String[2];
                    nes[0]=String.valueOf(event.getSceneX());
                    nes[1]=String.valueOf(event.getSceneY());
                    dibujarCirc(Double.parseDouble(String.valueOf(event.getSceneX())), Double.parseDouble(String.valueOf(event.getSceneY())));
                    asa.add(nes);
                    if(contador>0){
                        drawLines();
                        asa.clear();
                        asa.add(nes);
                    }
                    contador++;
                }
            }
        });
    }
    
    void dibujarRect(double x, double y){
        Circle circulito = new Circle(x,y,Grosor(),selectColor());
        root.getChildren().addAll(circulito);
    }
        
    void drawRectangle(){
        String[] pos1 = asa.get(0); // x
        String[] pos2 =asa.get(1);  // y
        double initx = Double.parseDouble(pos1[0]);
        double inity = Double.parseDouble(pos1[1]);
        double diffx = Math.abs(Double.parseDouble(pos1[0])-Double.parseDouble(pos2[0]));
        double diffy = Math.abs(Double.parseDouble(pos1[1])-Double.parseDouble(pos2[1]));
        if(initx> Double.parseDouble(pos2[0])){
            initx = Double.parseDouble(pos2[0]);
            inity = Double.parseDouble(pos2[1]);
            if(inity > Double.parseDouble(pos1[1])){
                inity = Double.parseDouble(pos1[1]);
            }else{
                inity = Double.parseDouble(pos2[1]); 
            }
        }
        if(inity > Double.parseDouble(pos2[1])){
            inity = Double.parseDouble(pos2[1]);
        }
        
        System.out.println(diffx);
        System.out.println(diffy);
        for(int x = 0;x<diffx;x++){
            for(int y = 0; y< diffy;y++ ){
                Circle circulito = new Circle(initx+x,inity+y,Grosor(),selectColor());
                root.getChildren().addAll(circulito);
            }
        }
    }
    
    @FXML
    private void cuadri() throws IOException{
        opcion="";
        sas=0;
        opcion="Trazos";
        System.out.println(opcion);
        asa.clear();
        dibujar();
    }
    
    @FXML
    private void linea() throws IOException{
        opcion="";
        sas=0;
        opcion="Lineas";
        System.out.println(opcion);
        dibujar();
    }
    
    void dibujarCirc(double x, double y){
        Circle circulito = new Circle(x,y,Grosor(),selectColor());
        root.getChildren().addAll(circulito);
    }
     
    void drawLines(){
        String[] pos1 = asa.get(0); // x
        String[] pos2 =asa.get(1);  // y
        System.out.println(pos1[0]);
        int posx1 = (int)Double.parseDouble(pos1[0]);
        int posx2 = (int)Double.parseDouble(pos2[0]);
        int posy1 = (int)Double.parseDouble(pos1[1]);
        int posy2 = (int)Double.parseDouble(pos2[1]);
        
        int cols = (int)root.getHeight();
        int rows = (int)root.getWidth();
        root.getWidth();
        Point[][] grid = new Point[rows][cols];
        for (int i = 0; i < rows; i++)
            for (int j = 0; j < cols; j++)
                grid[i][j] = new Point(i, j);
        List<Point> line = d.findLine(grid, posx1, posy1, posx2, posy2);
        re(grid,line);
    }
    void re(Point[][] grid, List<Point> line){
        int rows = grid.length;
        int cols = grid[0].length;
 
        System.out.println("\nPlot : \n");
 
        for (int i = 0; i < rows; i++)
        {
            for (int j = 0; j < cols; j++)
            {
                if (line.contains(grid[i][j])){
                    Circle circulito = new Circle(i,j,Grosor(),selectColor());
                    root.getChildren().addAll(circulito);
            }else{
                    }
            }
            System.out.println();
        }
    }
}
